const toggleBtn = document.getElementById("toggle-btn");
const todo = document.getElementById("input-text");
const todoList = document.getElementById("todo-container");
const itemsLeft = document.getElementById("items-left");
const allBtn = document.getElementById("all-btn");
const activeBtn = document.getElementById("active-btn");
const completedBtn = document.getElementById("completed-btn");
const clearCompletedBtn = document.getElementById("clear-btn");

todo.addEventListener('keypress', (e) => {
    if(e.key === "Enter"){
        var todoValue = todo.value;
        if(todoValue == ""){
            todo.setAttribute("placeholder", "Enter you todo")
        }else{
            createtodoElement(todoValue);
            todo.value = "";
            completedTask();
            deleteBtnClick();
            countItem();
        }
    }
})

//create todo element
function createtodoElement(val){
        //create li
        var li = document.createElement("li");
        li.classList.add("active-li");
        //create circle
        var circle = document.createElement("div");
        circle.classList.add("circle");
        //checkmark
        var checkmark = document.createElement("img");
        checkmark.setAttribute("src", "/images/icon-check.svg");
        checkmark.classList.add("checkmark-hide")
        circle.appendChild(checkmark);
        //create txt
        var txt = document.createElement("p");
        var newtodo = document.createTextNode(val);
        txt.appendChild(newtodo);
        //create cancel btn
        var cancelBtn = document.createElement("img")
        cancelBtn.setAttribute("src", "/images/icon-cross.svg");
        cancelBtn.classList.add("delete");
        //create nested li
        li.appendChild(circle);
        li.appendChild(txt);
        li.appendChild(cancelBtn);
        //insert neasted li to ul
        todoList.appendChild(li);
        
}
// completed task function
function completedTask(){
    const circles = document.getElementsByClassName("circle");
    var i;
    for ( i = 0; i < circles.length; i++){
        circles[i].onclick = function(){
           this.classList.toggle("checked");
           const p = this.nextElementSibling;
           p.classList.toggle("completed");
            if(this.classList.contains("checked")){
               this.parentElement.classList.remove("active-li");
               this.parentElement.classList.add("completed-li");
            }else{
            this.parentElement.classList.add("active-li");
            this.parentElement.classList.remove("completed-li");
            }
           countItem();
        }
    }
}
//Dynamically change the number of items left
function countItem(){
    const items = todoList.getElementsByClassName("active-li");
    const itemsCount = items.length;
    if(itemsCount == 0){
        itemsLeft.innerHTML = items.length + " item" + " left";    
    }else{
        itemsLeft.innerHTML = items.length + " items" + " left";
    }
}
//Toggle between Dark and Light theme
toggleBtn.addEventListener('click', (e) => {
   const icon = toggleBtn.getAttribute("src");
   if(icon == "/images/icon-moon.svg"){
       toggleBtn.setAttribute("src", "/images/icon-sun.svg");
       document.documentElement.setAttribute('data-theme', 'dark');
   }else{
    toggleBtn.setAttribute("src", "/images/icon-moon.svg");
    document.documentElement.setAttribute('data-theme', 'light');
   }
})
//click on All button
allBtn.addEventListener("click", () => {
     activeBtn.classList.remove("active");
     completedBtn.classList.remove("active");
     allBtn.classList.add("active");
     const completedItem = todoList.getElementsByClassName("completed-li");
     const activeItem = todoList.getElementsByClassName("active-li");
     var k;
     for( k = 0; k < completedItem.length; k++){
         completedItem[k].style.display = "flex";
     }
     var i;
     for( i = 0; i < activeItem.length; i++){
        activeItem[i].style.display = "flex";
    }
})
//Click on Active Button
activeBtn.addEventListener("click", () => {
     completedBtn.classList.remove("active");
     allBtn.classList.remove("active");
     activeBtn.classList.add("active");
     //hide completed item
     const completedItem = todoList.getElementsByClassName("completed-li");
     var k;
     for( k = 0; k < completedItem.length; k++){
         completedItem[k].style.display = "none";
     }

})
//Click on Completed Button
completedBtn.addEventListener("click", () => {
     activeBtn.classList.remove("active");
     allBtn.classList.remove("active");
     completedBtn.classList.add("active");
     const completedItem = document.getElementsByClassName("completed-li");
     var i;
     for(i = 0; i < completedItem.length; i++){
         completedItem[i].style.display = "flex";
     }
     const activeItem = document.getElementsByClassName("active-li");
     var k;
     for( k = 0; k < activeItem.length; k++){
         activeItem[k].style.display = "none";
     }
     
})
//Click on Delete Button
function deleteBtnClick(){
    const deleteBtns = document.getElementsByClassName("delete");
    var j;
    for( j = 0; j < deleteBtns.length; j++){
        deleteBtns[j].onclick = function(){
            const parent = this.parentElement;
            parent.remove();
            countItem();
        }
    }
}
//Click on Clear Completed Button
clearCompletedBtn.addEventListener("click", () => {
    const getCompletedItem = [].slice.call(document.getElementsByClassName("completed-li"));
    for(i = 0; i < getCompletedItem.length; i++){
       getCompletedItem[i].remove();
    }
})
countItem();
completedTask();
deleteBtnClick();